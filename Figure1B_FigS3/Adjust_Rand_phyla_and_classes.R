# -----------------------------------------------------------------------------------------
# Load libraries

setwd("some_directory")

library(data.table)
library(pdfCluster)
library(pracma)
library(ggplot2)
library(ggpubr)


# -----------------------------------------------------------------------------------------
# Import tables

ALL_levels=read.table("Taxonomy_plus_blastp.tsv",header=T,sep="\t", quote = "",row.names = NULL,stringsAsFactors = FALSE)
colnames(ALL_levels) <- c("HEADER","Protein","Phylum","Megacluster","Subphylum","Class","Order","suborder","Family",
                          "Subfamily","Genus",seq(0.1,8.0,by=0.1))

Total_clusters=read.table("Total_number_of_clusters.tsv",header=F,sep="\t", quote = "",row.names = NULL,stringsAsFactors = FALSE)

ALL_levels <- data.table(ALL_levels)

# -----------------------------------------------------------------------------------------
# Phyla and classes

Phylum <- as.data.frame(ALL_levels[ALL_levels$Phylum != "None"])
Phylum <- Phylum[-688,]

Megacluster <- as.data.frame(ALL_levels[ALL_levels$Megacluster != "None"])
Megacluster <- Megacluster[-670,]


#Class
Class <- as.data.frame(ALL_levels[ALL_levels$Class != "None"])
Class_Pisuviricota <- ALL_levels[ALL_levels$Megacluster == "Pisuviricota"]
Class_Pisuviricota <- as.data.frame(Class_Pisuviricota[Class_Pisuviricota$`1.1` == 1])
# remove singletons
Class_Pisuviricota <- Class_Pisuviricota[c(-348,-368,-433,-489),]

Class_Kitrinoviricota <- ALL_levels[ALL_levels$Megacluster == "Kitrinoviricota"]
Class_Kitrinoviricota <- as.data.frame(Class_Kitrinoviricota[Class_Kitrinoviricota$`1.1` == 2])
# remove singletons
Class_Kitrinoviricota <- Class_Kitrinoviricota[c(-92),]
##Class_Kitrinoviricota <- Class_Kitrinoviricota[c(-19,-93),]

Class_Negarnaviricota_Haploviricotina <- as.data.frame(ALL_levels[ALL_levels$Megacluster == "Negarnaviricota_Haploviricotina"])
# remove singletons
Class_Negarnaviricota_Haploviricotina <- Class_Negarnaviricota_Haploviricotina[c(-102,-144),]

Class_Negarnaviricota_Polyploviricotina <- as.data.frame(ALL_levels[ALL_levels$Megacluster == "Negarnaviricota_Polyploviricotina"])

Class_Duplornaviricota <- as.data.frame(ALL_levels[grepl("Duplornaviricota",ALL_levels$Megacluster)])
# remove singletons
Class_Duplornaviricota <- Class_Duplornaviricota[-62,]

Class_Lenarviricota <- as.data.frame(ALL_levels[grepl("Lenarviricota",ALL_levels$Megacluster)])
# remove singletons
Class_Lenarviricota <- Class_Lenarviricota[c(-237,-270,-340),]


# -----------------------------------------------------------------------------------------
# Calculate Adjusted Rand index

vec <- c(12:ncol(ALL_levels))
Adj_Rand <- vector()
Number_of_clusters <- vector()

# Phylum
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Phylum$Phylum, Phylum[,i])}
Adj_Rand_Phylum <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Phylum <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Phylum) <- c("cutoff")
Stats_Phylum$Adjusted_Rand_index <- Adj_Rand_Phylum
for (i in vec) {Number_of_clusters[i] <- length(uniq(Phylum[,i])$b)}
Stats_Phylum$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Megacluster
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Megacluster$Megacluster, Megacluster[,i])}
Adj_Rand_Megacluster <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Megacluster <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Megacluster) <- c("cutoff")
Stats_Megacluster$Adjusted_Rand_index <- Adj_Rand_Megacluster
for (i in vec) {Number_of_clusters[i] <- length(uniq(Megacluster[,i])$b)}
Stats_Megacluster$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]


# Class
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class$Class, Class[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Class_Duplornaviricota
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Duplornaviricota$Class, Class_Duplornaviricota[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Duplornaviricota[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Class_Negarnaviricota_Haploviricotina
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Negarnaviricota_Haploviricotina$Class, Class_Negarnaviricota_Haploviricotina[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Negarnaviricota_Haploviricotina[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Class_Negarnaviricota_Polyploviricotina
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Negarnaviricota_Polyploviricotina$Class, Class_Negarnaviricota_Polyploviricotina[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Negarnaviricota_Polyploviricotina[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Class_Lenarviricota
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Lenarviricota$Class, Class_Lenarviricota[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Lenarviricota[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]

# Class_Kitrinoviricota
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Kitrinoviricota$Class, Class_Kitrinoviricota[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Kitrinoviricota[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]


# Class_Pisuviricota
for (i in vec) {Adj_Rand[i] <- adj.rand.index(Class_Pisuviricota$Class, Class_Pisuviricota[,i])}
Adj_Rand_Class <- Adj_Rand[12:ncol(ALL_levels)]

Stats_Class <- data.frame(colnames(ALL_levels[,12:ncol(ALL_levels)]))
colnames(Stats_Class) <- c("cutoff")
Stats_Class$Adjusted_Rand_index <- Adj_Rand_Class
for (i in vec) {Number_of_clusters[i] <- length(uniq(Class_Pisuviricota[,i])$b)}
Stats_Class$Number_of_phyla <- Number_of_clusters[12:ncol(ALL_levels)]


# -----------------------------------------------------------------------------------------
# Phylum figures

RANGE <- c(5:15)

ggplot(Stats_Phylum[RANGE,], aes(group = 1))  + 
  geom_bar(aes(x=cutoff, y=Number_of_phyla),stat="identity", fill="tan1", colour="sienna3")+
  geom_line(aes(x=cutoff, y=Adjusted_Rand_index*max(Stats_Phylum$Number_of_phyla[RANGE])),stat="identity")+
  geom_text(aes(label=round(Adjusted_Rand_index,2), x=cutoff, y=Adjusted_Rand_index*max(Stats_Phylum$Number_of_phyla[RANGE]), group = 1), position = position_dodge(width = 1), vjust = -1.2, colour="black")+
  geom_text(aes(label=Number_of_phyla, x=cutoff, y=Number_of_phyla, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  scale_y_continuous(sec.axis = sec_axis(~./max(Stats_Phylum$Number_of_phyla[RANGE]), name = "Adjusted Rand Index"))+
  labs(x = "Clustering threshold (inflation value of MCL)", y= "Number of phyla")

ggplot(Stats_Megacluster[RANGE,], aes(group = 1))  +
  geom_bar(aes(x=cutoff, y=Number_of_phyla),stat="identity", fill="tan1", colour="sienna3")+
  geom_line(aes(x=cutoff, y=Adjusted_Rand_index*max(Stats_Phylum$Number_of_phyla[RANGE])),stat="identity")+
  geom_text(aes(label=round(Adjusted_Rand_index,2), x=cutoff, y=Adjusted_Rand_index*max(Stats_Phylum$Number_of_phyla[RANGE]), group = 1), position = position_dodge(width = 1), vjust = -1.2, colour="black")+
  geom_text(aes(label=Number_of_phyla, x=cutoff, y=Number_of_phyla, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  scale_y_continuous(sec.axis = sec_axis(~./max(Stats_Phylum$Number_of_phyla[RANGE]), name = "Adjusted Rand Index"))+
  labs(x = "Clustering threshold (inflation value of MCL)", y= "Number of phyla")


Stats_Phylum_merged <- cbind(Stats_Phylum,Stats_Megacluster,Total_clusters)
Stats_Phylum_merged[,c(3,4)] <- NULL
names(Stats_Phylum_merged) <- c("cutoff","Adjusted_Rand_index1","Adjusted_Rand_index2","Number_of_phyla", "Total_number_of_clusters")
Stats_Phylum_merged$Adjusted_Rand_index_final <- pmax(Stats_Phylum_merged$Adjusted_Rand_index1, Stats_Phylum_merged$Adjusted_Rand_index2)
Stats_Phylum_merged$Adjusted_Rand_index1 <- NULL
Stats_Phylum_merged$Adjusted_Rand_index2 <- NULL
Stats_Phylum_merged$remaining <- Stats_Phylum_merged$Total_number_of_clusters - Stats_Phylum_merged$Number_of_phyla


ggplot(Stats_Phylum_merged[RANGE,], aes(group = 1))  + 
  geom_bar(aes(x=cutoff, y=Total_number_of_clusters),stat="identity", fill="#A7F3B9", colour="black")+
  geom_bar(aes(x=cutoff, y=Number_of_phyla),stat="identity", fill="#9792E3", colour="black")+
  geom_line(aes(x=cutoff, y=Adjusted_Rand_index_final*max(Stats_Phylum_merged$Total_number_of_clusters[RANGE])),stat="identity")+
  geom_text(aes(label=round(Adjusted_Rand_index_final,2), x=cutoff, y=Adjusted_Rand_index_final*max(Stats_Phylum_merged$Total_number_of_clusters[RANGE]), group = 1), position = position_dodge(width = 1), vjust = -1.2, colour="black")+
  geom_text(aes(label=Number_of_phyla, x=cutoff, y=Number_of_phyla, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  geom_text(aes(label=remaining, x=cutoff, y=Total_number_of_clusters, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  scale_y_continuous(sec.axis = sec_axis(~./max(Stats_Phylum_merged$Total_number_of_clusters[RANGE]), name = "Adjusted Rand Index"))+
  labs(x = "Clustering threshold (inflation value of MCL)", y= "Number of clusters")


# -----------------------------------------------------------------------------------------
# Class figures (repeat for each class individually)

RANGE <- c(10:35)

ggplot(Stats_Class[RANGE,], aes(group = 1))  + 
  geom_bar(aes(x=cutoff, y=Number_of_phyla),stat="identity", fill="tan1", colour="sienna3")+
  geom_line(aes(x=cutoff, y=Adjusted_Rand_index*max(Stats_Class$Number_of_phyla[RANGE])),stat="identity")+
  geom_text(aes(label=round(Adjusted_Rand_index,2), x=cutoff, y=Adjusted_Rand_index*max(Stats_Class$Number_of_phyla[RANGE]), group = 1), position = position_dodge(width = 1), vjust = 1, colour="black")+
  geom_text(aes(label=Number_of_phyla, x=cutoff, y=Number_of_phyla, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  scale_y_continuous(sec.axis = sec_axis(~./max(Stats_Class$Number_of_phyla[RANGE]), name = "Adjusted Rand Index"))+
  labs(x = "Clustering threshold (inflation value of MCL)", y= "Number of clusters")



Stats_Phylum_merged <- cbind(Stats_Class,Stats_Class,Total_clusters)
Stats_Phylum_merged[,c(3,4)] <- NULL
names(Stats_Phylum_merged) <- c("cutoff","Adjusted_Rand_index1","Adjusted_Rand_index2","Number_of_phyla", "Total_number_of_clusters")
Stats_Phylum_merged$Adjusted_Rand_index_final <- pmax(Stats_Phylum_merged$Adjusted_Rand_index1, Stats_Phylum_merged$Adjusted_Rand_index2)
Stats_Phylum_merged$Adjusted_Rand_index1 <- NULL
Stats_Phylum_merged$Adjusted_Rand_index2 <- NULL
Stats_Phylum_merged$remaining <- Stats_Phylum_merged$Total_number_of_clusters - Stats_Phylum_merged$Number_of_phyla

ggplot(Stats_Phylum_merged[RANGE,], aes(group = 1))  + 
  geom_bar(aes(x=cutoff, y=Total_number_of_clusters),stat="identity", fill="#A7F3B9", colour="black")+
  geom_bar(aes(x=cutoff, y=Number_of_phyla),stat="identity", fill="#9792E3", colour="black")+
  geom_line(aes(x=cutoff, y=Adjusted_Rand_index_final*max(Stats_Phylum_merged$Total_number_of_clusters[RANGE])),stat="identity")+
  geom_text(aes(label=round(Adjusted_Rand_index_final,2), x=cutoff, y=Adjusted_Rand_index_final*max(Stats_Phylum_merged$Total_number_of_clusters[RANGE]), group = 1), position = position_dodge(width = 1), vjust = -1.2, colour="black")+
  geom_text(aes(label=Number_of_phyla, x=cutoff, y=Number_of_phyla, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  geom_text(aes(label=remaining, x=cutoff, y=Total_number_of_clusters, group = 1), position = position_dodge(width = 1), vjust = 1.2, colour="black")+
  scale_y_continuous(sec.axis = sec_axis(~./max(Stats_Phylum_merged$Total_number_of_clusters[RANGE]), name = "Adjusted Rand Index"))+
  labs(x = "Clustering threshold (inflation value of MCL)", y= "Number of clusters")

